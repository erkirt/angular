import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {PersonListComponent} from "./person-list/person-list.component";
import {PersonAddComponent} from "./person-add/person-add.component";
import {PersonByIdComponent} from "./person-by-id/person-by-id.component";

const routes : Routes = [
  {path: "person-list", component: PersonListComponent},
  {path: "person-form", component: PersonAddComponent},
  {path: "person-by-id/:id", component: PersonByIdComponent},
  {path: '', redirectTo: 'person-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
