import { Component, OnInit } from '@angular/core';
import {Person} from "../person";
import {PersonService} from "../person.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
export class PersonListComponent implements OnInit {

  public people: Person[] = [];

  constructor(private personService : PersonService,
              private router: Router) {}



  ngOnInit() {
    this.getPeople();
  }

  public getPeople() : void{
    this.personService.getPeople().subscribe(
      (response: Person[]) => {
        this.people = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  updatePerson(id: number | undefined){
    this.router.navigate(['person-by-id', id])
  }

}
