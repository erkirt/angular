import { Component, OnInit } from '@angular/core';
import {Person} from "../person";
import {PersonService} from "../person.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {

  person: Person = new Person();

  constructor(private personService: PersonService,
              private router: Router) { }

  ngOnInit(): void {
  }

  savePerson(){
    this.personService.addPeople(this.person).subscribe( (data: any) =>{
        console.log(data);
        this.goToPersonList();
      },
        (error: any) => console.log(error));
  }

  goToPersonList(){
    this.router.navigate(['/person-list']);
  }

  onSubmit(){
    console.log(this.person);
    this.savePerson();
  }

}
