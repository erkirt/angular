import { Person } from './person';
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";

@Injectable(
  {
    providedIn : "root"
  }
)
export class PersonService{
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getPeople(): Observable<Person[]>{
    return this.http.get<Person[]>(this.apiServerUrl+'/person/all')
  }
  public addPeople(person : Person): Observable<Person[]>{
    return this.http.post<Person[]>(this.apiServerUrl+'/person/add', person)
  }
  public getPersonById(id: number | undefined): Observable<Person[]>{
    return this.http.get<Person[]>(this.apiServerUrl+'/person/id')
  }
  public UpdatePersonById(person: Person, id: number | undefined): Observable<Person[]>{
    return this.http.put<Person[]>(this.apiServerUrl+'/person/'+id, person)
  }
  public DeletePersonById(person: number | undefined): Observable<Person[]>{
    return this.http.delete<Person[]>(this.apiServerUrl+'/person/id')
  }
}
