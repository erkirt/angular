import {Component, OnInit} from '@angular/core';
import {Person} from "./person";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {PersonService} from "./person.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit{
  title = 'Ergo Ergo project';

  public people: Person[] = [];

  constructor(private personService : PersonService) {}

  ngOnInit() {
    this.getPeople();
  }

  public getPeople() : void{
    this.personService.getPeople().subscribe(
      (response: Person[]) => {
        this.people = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

}
