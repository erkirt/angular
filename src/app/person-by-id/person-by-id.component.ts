import { Component, OnInit } from '@angular/core';
import {PersonService} from "../person.service";
import {Person} from "../person";
import {ActivatedRoute, Router} from "@angular/router";
import {error} from "@angular/compiler/src/util";

@Component({
  selector: 'app-person-by-id',
  templateUrl: './person-by-id.component.html',
  styleUrls: ['./person-by-id.component.css']
})
export class PersonByIdComponent implements OnInit {

  id: number | undefined;
  person: Person =  new Person();

  constructor(private personService: PersonService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.personService.getPersonById(this.id).subscribe(data => {
      console.log(data)
      // @ts-ignore
      this.person = data;
    }, error => console.log(error))
  }

  updatePerson(){
    this.personService.UpdatePersonById(this.person,this.id).subscribe(data =>{
      this.goToPersonList();
    }
    , error => console.log(error))
  }

  onSubmit(){
    this.updatePerson();
  }

  goToPersonList(){
    this.router.navigate(['/person-list']);
  }

}
