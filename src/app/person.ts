export class Person{
  id: number | undefined;
  firstname: string | undefined;
  lastname: string | undefined;
  gender: string | undefined;
  birthday: string | undefined;
}
